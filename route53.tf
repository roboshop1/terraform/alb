resource "aws_route53_record" "www" {
  zone_id = "Z06359783V969HYMH8KRR"
  name    = "frontend-prod.devops46.online"
  type    = "CNAME"
  ttl     = "5"
  records = [aws_lb.alb.dns_name]
}