data "aws_vpc" "vpc"{
  id = data.terraform_remote_state.vpc.outputs.VPC_ID
}

data "aws_instance" "instances"{
  instance_tags = {
    Name = "frontend-prod"
  }
}